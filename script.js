// Отримати за допомогою модального вікна браузера два числа.
//     Отримати за допомогою модального вікна браузера математичну операцію, яку потрібно виконати. Сюди може бути введено +, -, *, /.
// Створити функцію, в яку передати два значення та операцію.
//     Вивести у консоль результат виконання функції.

let num1 = Number(
  validate(prompt("Enter first number"), "Please enter correct first number" )
);
let num2 = Number(
  validate(prompt("Enter second number"), "Please enter correct second number")
);
let sign = prompt("Enter math operation +, -, /, *");


function validate(number, message) {
  while (isNaN(number) || !number) {
    number = prompt(message);
  }
  return number;
}

function calculator(num1, num2, sign) {
  let result = 0;
  switch (sign) {
    case "+":
      result = num1 + num2;
      break;
    case "-":
      result = num1 - num2;
      break;
    case "*":
      result = num1 * num2;
      break;
    case "/":
      result = num1 / num2;
      break;
    default:
      return "Wrong operator";
  }
  return result;
}

console.log(calculator(num1, num2, sign));


// Recursion version
// function validate (number, message) {
//     if (isNaN(number) || !number) {
//         return  validate( prompt('Enter correct number'))
//
//     }
// }
